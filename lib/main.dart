import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:temporalbdd/geo_example/utils.dart';
import 'package:temporalbdd/gpsDB.dart';
import 'package:temporalbdd/platform.dart';
import 'package:tuple/tuple.dart';


import 'package:wakelock/wakelock.dart';
import 'package:path/path.dart';

import 'package:sqflite/sqflite.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  /// The variable operation is used to prevent to launch two experiments
  /// at the same time. For simplicity we do not use token and assume sequential
  /// accesses to the variable. Values:
  /// -1 : interrupting the ongoing experiment
  /// 0 : default value, no operations are currently done
  /// 1 : XP on sqlite on random values
  /// 2 : FLI on random values
  /// 3 : SQLite on constant values
  /// 4 : FLI on constant values
  int _operation=0;
  String _getNameXP(int nbOperation) {
    switch(nbOperation) {
      case -1: return 'Interrupting experiment...';
      case 0: return 'No experiment running';
      case 1: return 'SQL on Cabspotting';
      case 2: return 'SQL on PrivaMov';
      case 3: return 'FLAIR on Cabspotting';
      case 4: return 'FLAIR on PrivaMov';
      default: return 'Error on operation number';
    }
  }
  /// Directory where the results are stored
  static const _addResults = 'results/';

  @override
  void initState() {
    super.initState();
    /// The following line will enable the Android and iOS wakelock.
    Wakelock.enable();
  }


  void _writeFileFLI(Map<int, GPSDB> dataset, String fileName) async {
    File file = await localDirFile('',fileName);
    for (var entry in dataset.entries) {
      file.writeAsStringSync('${entry.key}: [[${entry.value.latM.getModelsAndTimestamps()}/${entry.value.lngM.getModelsAndTimestamps()}]]\n', mode: FileMode.append);
    }
    return;
  }

  String _getNameResultsFile(String xp) {
    return '$xp--${getTimestamp()}.txt';
  }

  void _interruptXP(){
    _operation = -1;
    setState(() {
    });
  }

  void testCabspottingSQL() async {
    if(_operation != 0) {
      return;
    }
    _operation = 1;
    setState(() {});
    debugPrint('Starting experiment with SQL on cabspotting');

    ///Opening file for writing results
    String resultsFileName = _getNameResultsFile('sql-cabspotting');
    final File outputFile = await localDirFile('${_addResults}SQLCabspotting/',resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\n");

    const sqliteDBFileName = 'sqlite_database.db';

    ///opening the sqlite database
    WidgetsFlutterBinding.ensureInitialized();
    await deleteDatabase(join(await getDatabasesPath(), sqliteDBFileName));
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), sqliteDBFileName),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE RDpoints(uniqueindex INTEGER PRIMARY KEY, user INTEGER, timestamp REAL, lat REAL, lng REAL)");
      },
      version: 1,
    );
    final Database db = await database;
    var dbBatch = db.batch();


    Map<int, List<Tuple3<double, double, double>>> gpsDataset = {};
    final path = await localPath;
    try {
      gpsDataset = readCSV('$path/datasets/cabspotting');
    }
    catch (e) {
      debugPrint(e.toString());
      return;
    }

    int index = 0;
    for (var entry in gpsDataset.entries) {
      for (var point in entry.value.reversed) {
        /// Adding in SQL
        dbBatch.insert(
          'RDpoints',
          {'uniqueindex': index,
            'user': entry.key,
            'timestamp': point.item1,
            'lat': point.item2,
            'lng': point.item3},
          conflictAlgorithm: ConflictAlgorithm.replace,);
        index++;
      }

      /// commit for each user
      await dbBatch.commit(noResult: true);
      dbBatch = db.batch();
    }
    var file = File(join(await getDatabasesPath(), sqliteDBFileName));
    var fileLength = (await file.length()) / 1000000;
    debugPrint("DB SQLite file size: $fileLength Mb");
    outputFile.writeAsString("$index\t$fileLength\n", mode: FileMode.append);
    db.close();
    await deleteDatabase(join(await getDatabasesPath(), sqliteDBFileName));

    _operation = 0;
    setState(() {});
  }

  void testPrivamovSQL() async {
    if(_operation != 0) {
      return;
    }
    _operation = 2;
    setState(() {});
    debugPrint('Starting experiment with SQL on privamov');

    ///Opening file for writing results
    String resultsFileName = _getNameResultsFile('sql-privamov');
    final File outputFile = await localDirFile('${_addResults}SQLPrivamov/',resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\n");

    const sqliteDBFileName = 'sqlite_database.db';

    ///opening the sqlite database
    WidgetsFlutterBinding.ensureInitialized();
    await deleteDatabase(join(await getDatabasesPath(), sqliteDBFileName));
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), sqliteDBFileName),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE RDpoints(uniqueindex INTEGER PRIMARY KEY, user INTEGER, timestamp REAL, lat REAL, lng REAL)");
      },
      version: 1,
    );
    final Database db = await database;
    var dbBatch = db.batch();


    Map<int, List<Tuple3<double, double, double>>> gpsDataset = {};
    final path = await localPath;
    try {
      gpsDataset = readCSV('$path/datasets/privamov-gps');
    }
    catch (e) {
      debugPrint(e.toString());
      return;
    }

    int index = 0;
    for (var entry in gpsDataset.entries) {
      for (var point in entry.value) {
        /// Adding in SQL
        dbBatch.insert(
          'RDpoints',
          {'uniqueindex': index,
            'user': entry.key,
            'timestamp': point.item1,
            'lat': point.item2,
            'lng': point.item3},
          conflictAlgorithm: ConflictAlgorithm.replace,);
        index++;
      }

      /// commit for each user
      await dbBatch.commit(noResult: true);
      dbBatch = db.batch();
    }
    var file = File(join(await getDatabasesPath(), sqliteDBFileName));
    var fileLength = (await file.length()) / 1000000;
    debugPrint("DB SQLite file size: $fileLength Mb");
    outputFile.writeAsString("$index\t$fileLength\n", mode: FileMode.append);
    db.close();
    await deleteDatabase(join(await getDatabasesPath(), sqliteDBFileName));

    _operation = 0;
    setState(() {});
  }

  void testPrivamovSQL_() async {
    if(_operation != 0) {
      return;
    }
    _operation = 2;
    setState(() {});
    debugPrint('Starting experiment with SQL on privamov');

    ///Opening file for writing results
    String resultsFileName = _getNameResultsFile('sql-privamov');
    final File outputFile = await localDirFile('${_addResults}SQLPrivamov/',resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\n");

    const sqliteDBFileName = 'sqlite_database.db';

    ///opening the sqlite database
    WidgetsFlutterBinding.ensureInitialized();
    await deleteDatabase(join(await getDatabasesPath(), sqliteDBFileName));
    final Future<Database> database = openDatabase(
      join(await getDatabasesPath(), sqliteDBFileName),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE RDpoints(uniqueindex INTEGER PRIMARY KEY, user INTEGER, timestamp REAL, lat REAL, lng REAL)");
      },
      version: 1,
    );
    final Database db = await database;
    var dbBatch = db.batch();


    // Map<int,GPSDB> gpsDataset = {};
    final path = await localPath;
    final datasetFileName = '$path/datasets/privamov-gps';
    final datasetFile = File(datasetFileName);
    List<String> words;
    // int userID;
    // GPSDB userModel;

    int index = 0;

    Stream<String> lines = datasetFile.openRead()
        .transform(utf8.decoder)       // Decode bytes to UTF-8.
        .transform(const LineSplitter());    // Convert stream to individual lines.
    try {
      await for (var line in lines) {
        words = line.split(';');
        // userID = int.parse(words[0]);
        // userModel = gpsDataset.putIfAbsent(userID, () => GPSDB());
        // userModel.add(double.parse(words[3]), double.parse(words[1]), double.parse(words[2]));

        dbBatch.insert(
          'RDpoints',
          {'uniqueindex': index,
            'user': int.parse(words[0]),
            'timestamp': double.parse(words[3]),
            'lat': double.parse(words[1]),
            'lng': double.parse(words[2])},
          conflictAlgorithm: ConflictAlgorithm.replace,);
        index++;
        if (index % 10000 == 0) {
          await dbBatch.commit(noResult: true);
          dbBatch = db.batch();
        }
      }
      debugPrint('Dataset privamov is now closed.');
    }
    catch (e) {
      _operation = 0;
      setState(() {});
      debugPrint('Error while loading the dataset: $e');
      debugPrint('Experiment on privamov full: aborted');
      return;
    }
    var file = File(join(await getDatabasesPath(), sqliteDBFileName));
    var fileLength = (await file.length()) / 1000000;
    debugPrint("DB SQLite file size: $fileLength Mb");
    outputFile.writeAsString("$index\t$fileLength\n", mode: FileMode.append);
    db.close();
    await deleteDatabase(join(await getDatabasesPath(), sqliteDBFileName));

    _operation = 0;
    setState(() {});
  }



  void testCabspottingFLAIR() async {
    if(_operation != 0) {
      return;
    }
    _operation = 3;
    setState(() {});
    debugPrint('Starting experiment with FLAIR on cabspotting');
    const fliDBFileName = 'fli_database_cabspotting';

    ///Opening file for writing results
    String resultsFileName = _getNameResultsFile('flair-cabspotting');
    final File outputFile = await localDirFile('${_addResults}FLAIRCabspotting/',resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\n");



    Map<int, List<Tuple3<double, double, double>>> gpsDataset = {};
    final path = await localPath;
    try {
      gpsDataset = readCSV('$path/datasets/cabspotting');
    }
    catch (e) {
      debugPrint(e.toString());
      return;
    }

    int index = 0;
    Map<int, GPSDB> datasetFLAIR = {};
    GPSDB gpsdb;
    for (var entry in gpsDataset.entries) {
      gpsdb = GPSDB(0,{'error':'0.001'});
      for (var point in entry.value.reversed) {
        /// Adding in FLAIR
        gpsdb.add(point.item1, point.item2, point.item3);
      }
      datasetFLAIR.putIfAbsent(entry.key, () => gpsdb);
    }
    _writeFileFLI(datasetFLAIR, fliDBFileName);
    var file = await localDirFile('',fliDBFileName);
    var fileLength = (await file.length()) / 1000000;
    debugPrint("DB FLAIR file size: $fileLength Mb");
    outputFile.writeAsString("$index\t$fileLength\n", mode: FileMode.append);

    _operation = 0;
    setState(() {});
  }



  void testPrivamovFLAIR() async {
    if(_operation != 0) {
      return;
    }
    _operation = 4;
    setState(() {});
    debugPrint('Starting experiment with FLAIR on privamov');
    const fliDBFileName = 'fli_database_privamov';

    debugPrint('Opening files...');
    ///Opening file for writing results
    String resultsFileName = _getNameResultsFile('flair-privamov');
    final File outputFile = await localDirFile('${_addResults}FLAIRPrivamov/',resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\n");
    debugPrint('Files opened.');



    debugPrint('Reading dataset...');

    int index = 0;
    Map<int, GPSDB> datasetFLAIR = {};
    GPSDB userModel;

    List<String> words;
    int userID;

    final path = await localPath;
    final datasetFileName = '$path/datasets/privamov-gps';
    final datasetFile = File(datasetFileName);
    Stream<String> lines = datasetFile.openRead()
        .transform(utf8.decoder)       // Decode bytes to UTF-8.
        .transform(const LineSplitter());    // Convert stream to individual lines.
    try {
      await for (var line in lines) {
        words = line.split(';');
        userID = int.parse(words[0]);
        userModel = datasetFLAIR.putIfAbsent(userID, () => GPSDB(0,{'error':'0.001'}));
        userModel.add(double.parse(words[3]), double.parse(words[1]), double.parse(words[2]));
      }
      debugPrint('Dataset privamov is now closed.');
    }
    catch (e) {
      _operation = 0;
      setState(() {});
      debugPrint('Error while loading the dataset: $e');
      debugPrint('Experiment on privamov full: aborted');
      return;
    }

    debugPrint('Dataset modeled.');
    _writeFileFLI(datasetFLAIR, fliDBFileName);
    debugPrint('Model printed');
    var file = await localDirFile('',fliDBFileName);
    var fileLength = (await file.length()) / 1000000;
    debugPrint("DB FLAIR file size: $fileLength Mb");
    outputFile.writeAsString("$index\t$fileLength\n", mode: FileMode.append);

    _operation = 0;
    setState(() {});
  }








  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              /// Launch XP: storing random values with sql
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                ),
                onPressed: testCabspottingSQL,
                child: const Text('XP: Storing cabspotting with SQLite'),
              ),

              /// Launch XP: storing random values with FLI
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                ),
                onPressed: testPrivamovSQL_,
                child: const Text('XP: Storing privamov with SQLite'),
              ),

              /// Launch XP: storing constant values with sql
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                ),
                onPressed: testCabspottingFLAIR,
                child: const Text('XP: Storing cabspotting with FLAIR'),
              ),

              /// Launch XP: storing constant values with sql
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                ),
                onPressed: testPrivamovFLAIR,
                child: const Text('XP: Storing privamov with FLAIR'),
              ),

              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  _getNameXP(_operation),
                  style: Theme.of(context).textTheme.headline5,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _interruptXP,
        tooltip: 'Interrupt experiment',
        child: const Icon(Icons.clear_rounded),
        backgroundColor: Colors.red,
      ),
    );
  }
}
